package TP3_DS1;

import java.util.HashMap;

public class Prcessor extends Thread {

	private String Pname;
	private MyBuffer myBuffer;
	private HashMap<Integer, String> wordsCounts;
	int i = 0;

	public Prcessor(String threadName, MyBuffer myBuffer) {
		super();
		this.Pname = threadName;
		this.wordsCounts = new HashMap<Integer, String>();
		this.myBuffer = myBuffer;
	}

	public void processe() throws InterruptedException {
		wordsCounts.put(++i, myBuffer.getWord());
	}

	public HashMap<Integer, String> getWordsCounts() {
		return wordsCounts;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			while (true) {
				processe();
				//if(i==4)break;//how to stop this thread

			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
