package TP3_DS1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Test {
	public static void main(String[] args) throws IOException {

		MyBuffer myBuffer = new MyBuffer(100);
		Lecteur lecteur = new Lecteur("Lecteur_text.txt", "Lecteur", myBuffer);
		Prcessor prcessor = new Prcessor("Processor", myBuffer);

		lecteur.start();
		prcessor.start();
		while(lecteur.isAlive()||prcessor.isAlive()) {
			
		}
		//i need to find a condition to stop the processor after collect all words
		System.out.println("------------------prcessor wordsCounts-------------");
		HashMap<Integer, String> hashMap = prcessor.getWordsCounts();
		Set<Integer> set = hashMap.keySet();
		Iterator<Integer> iterator = set.iterator();

		while (iterator.hasNext()) {
			int i=iterator.next();
			System.out.println(hashMap.get(i));
		}

		// ------------------------------------------------

	}
}
