package TP3_DS1;

import java.util.LinkedList;

public class MyBuffer {
	private int size = 0;
	private int maxSize;
	private LinkedList<String> data;
	boolean setValue = false;

	public MyBuffer(int size) {
		this.maxSize = size;
		this.data = new LinkedList<String>();
	}

	public synchronized void addWord(String e) throws InterruptedException {

		if (setValue || size >= maxSize)
			wait();
		System.out.println("addWord: " + e);
		data.add(e);
		size++;
		setValue = true;
		notify();

	}

	public synchronized String getWord() throws InterruptedException {

		if (!setValue || size <= 0) {
			wait();
		}
		String string = data.get(size - 1);
		System.out.println("getWord: " + string);

		data.remove(size - 1);
		size--;
		setValue = false;
		notify();
		return string;
	}

	public LinkedList<String> getWords() {
		return this.data;
	}
}
