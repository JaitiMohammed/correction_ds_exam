package TP3_DS1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SuppressWarnings("unused")

public class Lecteur extends Thread {
	private String filename;
	private String threadNme;
	private MyBuffer buff;
	FileInputStream fis;
	int EOF = 9;

	public Lecteur(String filename, String threadNme, MyBuffer buff) throws IOException {
		this.filename = filename;
		this.threadNme = threadNme;
		this.buff = buff;
		fis = new FileInputStream(new File(filename));
	}

	public String readWord() throws IOException {
		
		String str = "";
		byte[] _byte_ = new byte[1];

		int i = 0;
		while ((i = fis.read(_byte_)) != -1 && Character.toString((char) _byte_[0]).matches("\\w")) {

			str += Character.toString((char) _byte_[0]);
		}
		EOF = i;
		return str;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		try {
			while (true) {
				this.buff.addWord(readWord());
				if (EOF == -1)
					break;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}